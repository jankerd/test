chapter 1 Gotta
We are unknown to ourselves, we knowers: and with good reason. We
have never looked for ourselves, – so how are we ever supposed to find ourselves?
How right is the saying: ‘Where your treasure is, there will your
heart be also’;1 our treasure is where the hives of our knowledge are. As
born winged-insects and intellectual honey-gatherers we are constantly
making for them, concerned at heart with only one thing – to ‘bring something
home’. As far as the rest of life is concerned, the so-called ‘experiences’,
– who of us ever has enough seriousness for them? or enough time?
I fear we have never really been ‘with it’ in such matters: our heart is
simply not in it – and not even our ear! On the contrary, like somebody
divinely absent-minded and sunk in his own thoughts who, the twelve
strokes of midday having just boomed into his ears, wakes with a start and
wonders ‘What hour struck?’, sometimes we, too, afterwards rub our ears
and ask, astonished, taken aback, ‘What did we actually experience then?’
or even, ‘Who are we, in fact?’ and afterwards, as I said, we count all twelve
reverberating strokes of our experience, of our life, of our being – oh! and
lose count . . . We remain strange to ourselves out of necessity, we do not
understand ourselves, we must confusedly mistake who we are, the motto2
‘everyone is furthest from himself ’ applies to us for ever, – we are not
‘knowers’ when it comes to ourselves . . .

chapter 2 Catch
– My thoughts on the descent of our moral prejudices – for that is what
this polemic is about – were first set out in a sketchy and provisional way
in the collection of aphorisms entitled Human, All Too Human. A Book
for Free Spirits,
3 which I began to write in Sorrento during a winter that
enabled me to pause, like a wanderer pauses, to take in the vast and dangerous
land through which my mind had hitherto travelled. This was in
the winter of 1876–7; the thoughts themselves go back further. They were
mainly the same thoughts which I shall be taking up again in the present
essays – let us hope that the long interval has done them good, that they
have become riper, brighter, stronger and more perfect! The fact that I
still stick to them today, and that they themselves in the meantime have
stuck together increasingly firmly, even growing into one another and
growing into one, makes me all the more blithely confident that from the
first, they did not arise in me individually, randomly or sporadically but
as stemming from a single root, from a fundamental will to knowledge
deep inside me which took control, speaking more and more clearly and
making ever clearer demands. And this is the only thing proper for a
philosopher. We have no right to stand out individually: we must not
either make mistakes or hit on the truth individually. Instead, our
thoughts, values, every ‘yes’, ‘no’, ‘if ’ and ‘but’ grow from us with the
same inevitability as fruits borne on the tree – all related and referring to
one another and a testimonial to one will, one health, one earth, one sun.
– Do you like the taste of our fruit? – But of what concern is that to the
trees? And of what concern is it to us philosophers? . . .

chapter 3 Them
With a characteristic scepticism to which I confess only reluctantly –
it relates to morality and to all that hitherto on earth has been celebrated
as morality –, a scepticism which sprang up in my life so early, so unbidden,
so unstoppably, and which was in such conflict with my surroundings,
age, precedents and lineage that I would almost be justified in calling
it my ‘a priori’, – eventually my curiosity and suspicion were bound to fix
on the question of what origin our terms good and evil actually have.
Indeed, as a thirteen-year-old boy, I was preoccupied with the problem of
the origin of evil: at an age when one’s heart was ‘half-filled with childish
games, half-filled with God’,4 I dedicated my first literary childish game,
my first philosophical essay, to this problem – and as regards my ‘solution’
to the problem at that time, I quite properly gave God credit for it and
made him the father of evil. Did my ‘a priori’ want this of me? That new,
immoral, or at least immoralistic ‘a priori’: and the oh-so-anti-Kantian, so
enigmatic ‘categorical imperative’5 which spoke from it and to which I
have, in the meantime, increasingly lent an ear, and not just an ear? . . .
Fortunately I learnt, in time, to separate theological from moral prejudice
and I no longer searched for the origin of evil beyond the world. Some
training in history and philology, together with my innate fastidiousness
with regard to all psychological problems, soon transformed my problem
into another: under what conditions did man invent the value judgments
good and evil? and what value do they themselves have? Have they up to
now obstructed or promoted human flourishing? Are they a sign of distress,
poverty and the degeneration of life? Or, on the contrary, do they
reveal the fullness, strength and will of life, its courage, its confidence, its
future? To these questions I found and ventured all kinds of answers of
my own, I distinguished between epochs, peoples, grades of rank between
individuals, I focused my inquiry, and out of the answers there developed
new questions, investigations, conjectures, probabilities until I had my
own territory, my own soil, a whole silently growing and blossoming
world, secret gardens, as it were, the existence of which nobody must be
allowed to suspect . . . Oh! how happy we are, we knowers, provided we
can keep quiet for long enough! . . .

chapter 4 All!
I was given the initial stimulation to publish something about my
hypotheses on the origin of morality by a clear, honest and clever, even
too-clever little book, in which I first directly encountered the back-tofront
and perverse kind of genealogical hypotheses, actually the English
kind, which drew me to it – with that power of attraction which everything
contradictory and antithetical has. The title of the little book was
The Origin of the Moral Sensations; its author was Dr Paul Rée; the year
of its publication 1877. I have, perhaps, never read anything to which I
said ‘no’, sentence by sentence and deduction by deduction, as I did to
this book: but completely without annoyance and impatience. In the work
already mentioned which I was working on at the time, I referred to passages
from this book more or less at random, not in order to refute them –
what business is it of mine to refute! – but, as befits a positive mind, to
replace the improbable with the more probable and in some circumstances
to replace one error with another. As I said, I was, at the time,
bringing to the light of day those hypotheses on descent to which these
essays are devoted, clumsily, as I am the first to admit, and still inhibited
because I still lacked my own vocabulary for these special topics, and with
a good deal of relapse and vacillation. In particular, compare what I say
about the dual prehistory of good and evil in Human, All Too Human,
section 45 (namely in the sphere of nobles and slaves); likewise section 136
on the value and descent of ascetic morality; likewise sections 96 and 99
and volume II, section 89 on the ‘Morality of Custom’, that much older
and more primitive kind of morality which is toto coelo6 removed from
altruistic evaluation (which Dr Rée, like all English genealogists, sees as
the moral method of valuation as such); likewise section 92, The Wanderer,
section 26, and Daybreak, section 112, on the descent of justice as a
balance between two roughly equal powers (equilibrium as the precondition
for all contracts and consequently for all law); likewise The
Wanderer, sections 22 and 33 on the descent of punishment, the deterrent
[terroristisch] purpose of which is neither essential nor inherent (as Dr Rée
thinks: – instead it is introduced in particular circumstances and is always
incidental and added on).
chapter 5 Pokemon!
chapter ()__()
chapter (='.'=)
chapter (")__(")

