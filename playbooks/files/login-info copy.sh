#!/bin/bash
#put file here /etc/profile.d/login-info.sh and remove /etc/motd
echo -e "\e[30m\e[1m
   _____  _____   ______         _______  ______  _____  
  / ____||  __ \ |  ____|    /\ |__   __||  ____||  __ \ 
 | |  __ | |__) || |__      /  \   | |   | |__   | |__) |
 | | |_ ||  _  / |  __|    / /\ \  | |   |  __|  |  _  / 
 | |__| || | \ \ | |____  / ____ \ | |   | |____ | | \ \ 
  \_____||_|  \_\|______|/_/    \_\|_|   |______||_|  \_\\
\e[0m" "\e[34m\e[1m
  _________                                                 
 |_________|
\e[0m\e[97m 
Welcome to\e[91m `hostname`\e[91m\e[97m
You are logged in as:\e[91m `whoami`\e[0m
"
